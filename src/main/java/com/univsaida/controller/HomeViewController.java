package com.univsaida.controller;

import com.univsaida.exception.BookException;
import com.univsaida.model.Book;
import com.univsaida.model.Cart;
import com.univsaida.service.BooksService;
import com.univsaida.view.Template;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class HomeViewController {

    private BooksService service;
    private Cart cart ;
    private Template alert ;
    private ObservableList<Book> booksList;

    public HomeViewController (BooksService service,Cart cart,Template alert){
        this.service = service;
        this.cart = cart;
        this.setBooksList(service.getBooks());
        this.alert = alert;
    }

    public void searchMethod(String search, String searchWith) {
        ObservableList<Book> all;
        all = this.service.getTable().getItems();
        List<Book> filtr=null;
        switch (searchWith) {
            case "Mots Clés":
                filtr = this.service.getBooksInTable().filtered(book -> book.getKeyWords().get(0).toLowerCase().contains(search.toLowerCase()));
                break;
            case "Titre":
                filtr = this.service.getBooksInTable().filtered(book -> book.getTitle().toLowerCase().contains(search.toLowerCase()));
                break;
            case "Auteur":
                filtr =this.service.getBooksInTable().filtered(book -> book.getAuthors().get(0).toLowerCase().contains(search.toLowerCase()));
                break;
        }
        all.clear();
        all.addAll(filtr);
    }// void end

    public void add(){
        if (this.service.getTable().getSelectionModel().getSelectedItem() != null) {
            if (this.service.getTable().getSelectionModel().getSelectedItem().isAvailable()) {
                try {
                    List<Book> k = this.service.getBooksInCart();
                    k.add(this.service.getTable().getSelectionModel().getSelectedItem());
                    this.cart.setBooks(k);
                   this.service.getCartTable().getItems().add(this.service.getTable().getSelectionModel().getSelectedItem());
                    ObservableList<Book> all;
                    Book selected;
                    all = this.service.getTable().getItems();
                    selected = this.service.getTable().getSelectionModel().getSelectedItem();
                    all.remove(selected);
                    this.service.deleteBooksInTable(selected);
                } catch (BookException e1) {
                    this.alert.getAlert("error", new String[]{"Error", "Panier déja plein", "vous ne pouvez pas ajouter que 3 ouvrages !!"}).showAndWait();
                }
            } else {
                this.alert.getAlert("confirmation", new String[]{"ouvrage non disponible", "non disponible !!", "Si tu veux le recommande clique sur 'Recommande' ", "Recommande", "Non"}).showAndWait();
            }
        }
    }// end add


    public void setTable(TableView<Book> table) {
        this.service.setTable(table);
    }

    public TableView<Book> getTable() {
        return this.service.getTable();
    }

}
