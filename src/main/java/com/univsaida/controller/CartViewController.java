package com.univsaida.controller;

import com.univsaida.exception.BookException;
import com.univsaida.model.Book;
import com.univsaida.model.Cart;
import com.univsaida.service.BooksService;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class CartViewController {

    private BooksService service;
    private Cart cart;
    private ObservableList<Book> booksListInCart;


    public CartViewController(BooksService service, Cart cart) {
        this.service = service;
        this.cart = cart;
        this.setBooksListInCart(this.service.getBooksInCart());
    }


    public void delete() {
        ObservableList<Book> all;
        Book selected;
        if (this.service.getCartTable().getSelectionModel().getSelectedItem() != null) {
            all = this.service.getCartTable().getItems();
            selected = this.service.getCartTable().getSelectionModel().getSelectedItem();
            List<Book> k = this.service.getBooksInCart();
            k.remove(selected);
            try {
               this.cart.setBooks(k);
            } catch (BookException e) {
                // e.printStackTrace();
            }
            all.remove(selected);
           this.service.getTable().getItems().add(selected);
            this.service.setBooksInTable(selected);
        }
    }// end delete

    public void setCartTable(TableView<Book> cartTable) {
        this.service.setCartTable(cartTable);
    }

    public TableView<Book> getCartTable() {
        return this.service.getCartTable();
    }


}
