package com.univsaida;

import com.univsaida.controller.CartViewController;
import com.univsaida.controller.HomeViewController;
import com.univsaida.model.Cart;
import com.univsaida.service.BooksService;
import com.univsaida.view.CartView;
import com.univsaida.view.HomeView;
import com.univsaida.view.Template;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends javafx.application.Application {
    
    public static void main(String[] args) throws IOException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Cart cart = new Cart();
        BooksService service = new BooksService(cart);
        Template template = new Template();
        HomeViewController homeCtrl = new HomeViewController(service, cart, template);
        CartViewController cartCtrl = new CartViewController(service, cart);
        CartView cartView = new CartView(cartCtrl, template);
        HomeView homeView = new HomeView(homeCtrl, cartView, template);

        primaryStage.setTitle("Gestion Bibliotheque");
        Scene scene = new Scene(homeView.getHomeView(), 910, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}
