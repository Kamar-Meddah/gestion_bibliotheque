package com.univsaida.service;

import com.univsaida.model.Book;
import com.univsaida.model.Cart;
import com.univsaida.repository.BooksRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BooksService {

    private Cart cartInstance;
    private ObservableList<Book> booksInTable;
    private TableView<Book> cartTable;
    private TableView<Book> table;

    public BooksService(Cart cartInstance) {
        booksInTable = FXCollections.observableArrayList();
        this.cartInstance = cartInstance;
        this.booksInTable.addAll(this.getBooks());
    }

    public void setBooksInTable (Book book){
        this.booksInTable.add(book);
    }

    public void deleteBooksInTable (Book book){
        this.booksInTable.remove(book);
    }

    public ObservableList<Book> getBooksInCart() {
        ObservableList<Book> booksInCart = FXCollections.observableArrayList();
        if (this.cartInstance.getBooks() != null) {
            booksInCart.addAll(this.cartInstance.getBooks());
        }
        return booksInCart;
    }//end method

    public ObservableList<Book> getBooks() {
        ObservableList<Book> books = FXCollections.observableArrayList();
        books.addAll(new BooksRepository().findAll());
        return books;
    }

}
