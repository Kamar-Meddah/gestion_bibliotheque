package com.univsaida.view;

import com.univsaida.controller.CartViewController;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartView {

    private Template template;
    private CartViewController controller;


    public CartView (CartViewController controller, Template template){
        this.controller=controller;
        this.template = template;
    }

    public Alert getCartTableView() {
        String[] columns = {"title", "authors", "docType","languages", "originalLanguages", "categories","resume"};
        this.controller.setCartTable(this.template.getBooksTableView(columns,this.controller.getBooksListInCart(),"Panier Vide ..! emprunter un livre maintenant !!"));
        Button remove = this.template.getButton("-");
        remove.setOnAction(e3 -> this.controller.delete());
        VBox vbox7 = new VBox(remove,new Separator(),this.controller.getCartTable());
        Alert alert = this.template.getAlert("warning",new String[]{"warning","Exception Dialog","Panier: "},vbox7);
        return alert;
    }

}
