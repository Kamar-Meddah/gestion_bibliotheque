package com.univsaida.view;

import com.univsaida.controller.HomeViewController;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeView {

    private HomeViewController controller;
    private CartView cartView;
    private Template template;

    public HomeView(HomeViewController controller, CartView cartView, Template template) {
        this.controller = controller;
        this.cartView = cartView;
        this.template = template;
    }

    private VBox getHeaderView( ) {
        Alert cart =this.cartView.getCartTableView();

        HBox hbox1 = new HBox(this.template.getLabel("** BienVenue Abonnée **", new int[]{20, 20, 5, 20}));
        HBox hbox2 = new HBox(this.template.getLabel(" Liste Des Ouvrages: ", new int[]{10, 20, 10, 5}));

        TextField search = this.template.getTextField("Rechecher ici");

        ChoiceBox<String> searchWith = this.template.getChoiceBox(new String[]{"Auteur", "Mots Clés", "Titre"});
        searchWith.setValue("Titre");

        Button searchBtn = this.template.getButton("Rechercher");
        Button cartBtn = this.template.getButton("Panier");
        Button add = this.template.getButton("+");

        HBox hBox6 = new HBox(search, searchWith, searchBtn, add, cartBtn);
        hBox6.setPadding(new Insets(20, 20, 20, 20));
        hBox6.setSpacing(10);

        add.setOnAction((e) -> this.controller.add());
        searchBtn.setOnAction((e4) -> this.controller.searchMethod(search.getText(), searchWith.getSelectionModel().getSelectedItem()));
        cartBtn.setOnAction(event -> cart.showAndWait());

        VBox header =new VBox(hbox1,new Separator(),hbox2,new Separator(),hBox6);
        return header;
    }

    private HBox getFooterView() {
        HBox hbox5 = new HBox(this.template.getButton(" Confirmer "));
        hbox5.setPadding(new Insets(10, 20, 10, 5));
        return hbox5;
    }

    public VBox getHomeView () {
        String[] columns = {"title", "authors", "docType", "editor", "collection", "isbnIssnEan", "format", "languages", "originalLanguages", "index", "categories", "keyWords", "resume","available"};
        this.controller.setTable(this.template.getBooksTableView(columns,this.controller.getBooksList()));
        VBox home = new VBox();
        home.getChildren().addAll(getHeaderView(), this.controller.getTable(),getFooterView());
        this.controller.searchMethod(" ", "Titre");
        return home;
    }

}
