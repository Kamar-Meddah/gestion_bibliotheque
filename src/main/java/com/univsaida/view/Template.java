package com.univsaida.view;

import com.univsaida.model.Book;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class Template {

    // Alert
    public Alert getAlert(String type, String[] args) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        if (type.toLowerCase() == "error") {
            alert.setAlertType(Alert.AlertType.ERROR);
            ;
        } else if (type.toLowerCase() == "confirmation") {
            alert.setAlertType(Alert.AlertType.CONFIRMATION);
            ;
        } else if (type.toLowerCase() == "warning") {
            alert.setAlertType(Alert.AlertType.WARNING);
            ;
        }
        alert.setTitle(args[0]);
        alert.setHeaderText(args[1]);
        alert.setContentText(args[2]);
        if (args.length > 3) {
            ButtonType button1 = new ButtonType(args[3]);
            alert.getButtonTypes().set(0, button1);
            if (args.length > 4) {
                ButtonType button2 = new ButtonType("Non", ButtonBar.ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().set(1, button2);
            }
        }
        return alert;
    }

    public Alert getAlert(String type, String[] args, VBox vbox) {
        Alert alert = this.getAlert(type, args);
        alert.getDialogPane().setContent(vbox);
        return alert;
    }

    //  Label
    public Label getLabel(String title, int[] args) {
        Label label = new Label(title);
        label.setPadding(new Insets(args[0], args[1], args[2], args[3]));
        return label;
    }

    public Label getLabel(String title, int arg) {
        Label label = new Label(title);
        label.setPadding(new Insets(arg));
        return label;
    }

    // Table
    private TableColumn<Book, String> createColumn(String name) {
        TableColumn<Book, String> column = new TableColumn<>(name);
        column.setPrefWidth(100);
        column.setCellValueFactory(new PropertyValueFactory<>(name));
        return column;
    }

    public TableView<Book> getBooksTableView(String[] columns, ObservableList<Book> data) {
        TableView<Book> table = new TableView<>();
        table.setItems(data);
        for (String column : columns) {
            table.getColumns().add(this.createColumn(column));
        }
        table.setPadding(new Insets(0, 10, 10, 10));
        return table;
    }

    public TableView<Book> getBooksTableView(String[] columns, ObservableList<Book> data, String placeholder) {
        TableView<Book> table = new TableView<>();
        table.setItems(data);
        for (String column : columns) {
            table.getColumns().add(this.createColumn(column));
        }
        table.setPadding(new Insets(0, 10, 10, 10));
        table.setPlaceholder(new Label(placeholder));
        return table;
    }

    public TextField getTextField(String title) {
        TextField text = new TextField();
        text.setPromptText(title);
        return text;
    }

    public ChoiceBox<String> getChoiceBox(String title []) {
        ChoiceBox<String> box = new ChoiceBox<>();
        for (String s : title) {
            box.getItems().add(s);
        }
        return box;
    }

    public Button getButton(String title) {
        Button btn = new Button(title);
        return btn;
    }

}
