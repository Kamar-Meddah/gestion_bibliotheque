package com.univsaida.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Book{
    private String title;
    private List<String> authors;
    private TypeDocumentEnum docType;
    private String editor;
    private String collection;
    private String isbnIssnEan;
    private String format;
    private List<String> languages;
    private List<String> originalLanguages;
    private String index;
    private List<String> categories;
    private List<String> keyWords;
    private String resume;
    private boolean available;

}
