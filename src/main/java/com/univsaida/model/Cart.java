package com.univsaida.model;

import com.univsaida.exception.BookException;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter

public class Cart {

    private List<Book> books;

    public void setBooks(List<Book> books) throws BookException {
        if (books == null) {
            throw new BookException("The books list must not be null");
        }
        if (books.size() > 3) {
            throw new BookException("The books list must contains 3 books maximum");
        }

        this.books = books;
    }

}
