package com.univsaida.model;

public enum TypeDocumentEnum {

    TEXTE_IMPRIMÉ("Texte Imprimé");

    private String type;

    TypeDocumentEnum(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
