package com.univsaida.repository;

import com.univsaida.model.Book;
import com.univsaida.model.TypeDocumentEnum;
import com.univsaida.service.BooksService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BooksRepository {
    private List <Book> books = new ArrayList<>();

    public List<Book> findAll(){
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("donnees.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.readLine(); // the first line contains titles
            while (line != null) {
                line = reader.readLine();
                if (line != null) {
                    String[] split = line.split(",");
                    Book book = new Book();
                    book.setTitle(split[0]);
                    List<String> authors = new ArrayList<>();
                    authors.add(split[1]);
                    book.setAuthors(authors);
                    book.setDocType(TypeDocumentEnum.valueOf(split[2].toUpperCase().replace(' ','_')));
                    book.setEditor(split[3]);
                    book.setCollection(split[4]);
                    book.setIsbnIssnEan(split[5]);
                    book.setFormat(split[6]);
                    List<String> langs = new ArrayList<>();
                    langs.add(split[7]);
                    book.setLanguages(langs);
                    List<String> originLangs = new ArrayList<>();
                    originLangs.add(split[8]);
                    book.setOriginalLanguages(originLangs);
                    book.setIndex(split[9]);
                    List<String> categories = new ArrayList<>();
                    categories.add(split[10]);
                    book.setCategories(categories);
                    List<String> keyWords = new ArrayList<>();
                    keyWords.add(split[11]);
                    book.setKeyWords(keyWords);
                    book.setResume(split[12]);
                    book.setAvailable(Boolean.parseBoolean(split[13]));
                    this.books.add(book);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.books;
    }
}
